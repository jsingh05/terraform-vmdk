provider "vsphere" {
  user           = "${var.vsphere_user}"
  password       = "${var.vsphere_password}"
  vsphere_server = "https://vcenter.lab.nwa.evtcorp.com"
}

resource "vsphere_virtual_disk" "myDisk" {
  size       = 2
  vmdk_path  = "test/test.vmdk"
  datastore  = "local"
}
